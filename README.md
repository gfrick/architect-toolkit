# Getting Started 

This project was created with [Create React App](https://github.com/facebook/create-react-app); which you can learn more about with the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Major Directories

### Components

Components that only accept props; there should be no state or hooks.

### Data

Locate data file entries here that are displayed as a top level topic.

### Layout

Components that control the layout and design of the primary site like Header, Content, Footer.

### Models

Locate data models and other anemic typescript interfaces here.

## Q & A

### Why don't you call a web service to get the data?
Because it isn't needed, it's easy enough to push a change via git at this point. Later, the files can easily be used to populate a datastore for a service.

### Why does this exist?
In many engineering disciplines we make sure that important details aren't missed by using checklists. Experience with my own teams and projects has led me to believe it would be helpful to build out a resource to teams can use to ensure completeness.

When I'm starting a new project, or finishing up an existing project, how can I make sure that I've covered
everything important.

 For example, are you handling exceptions or letting them silently slip away?
 When running a batch job, keep some kind of receipt for you work.

Quality -> Error Handling: This comes down to "how do you handle errors. Do you treat exceptions
as exceptions and errors as errors.

Quality -> Data Processing: Which of your data processing actions are idempotent - can more of them be? How do you
know what failed and what succeeded? Failures should be traceable.

Tech Stack -> Innovation Balance: Using a new framework vs using reliable technology; you have to find the balance
and follow your maturity model. If you don't have a maturity model that is a great place to start. Technology that
is too old can have problems with security, finding developers, and providing features. Technology that is too new can
be buggy or unstable while costing time based on the learning curve.

SCM -> Decision Record: When the team makes decisions together or upgrade the maturity model then record it directly
in the source code including when it happened, what was decided, and why.

Tech Stack -> Contribute guide: Provide developers with a contribution guide that lays out how code should be added,
the PR process, etc. If this is short, it can live in the readme. If a project is deprecated your contribution guide
can explain this.
