
export interface IResource {
    url: string;
    description: string;
}

export interface ITaskItem {
    id: number;
    description: string;
    resources?: IResource[];
}

export interface ITaskTopic {
    id: number;
    description: string;
    items?: ITaskItem[];
}
