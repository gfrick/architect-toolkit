import React from 'react';
import '../App.css';
import {IResource} from "../models/task-item";

export interface ITaskItemProps {
    resources?: IResource[];
}

function TaskItemResources(props: ITaskItemProps) {

    if (!props.resources || props.resources.length === 0) {
        return <></>;
    }

    return (<div className="card-list">
        {
            props.resources.map((resource, idx) =>
                <div><a href={resource.url} target="_">{resource.description}</a></div>
            )
        }
    </div>);
}

export default TaskItemResources;
